import { HttpParams, HttpClient } from "@angular/common/http";

export class UploadAdapter {
    constructor(
      private loader,
      public url:string,
      private http:HttpClient
      ) {
      this.loader=loader;
    }
//the uploadFile method use to upload image to your server
  uploadFile(file,url?:string,user?:string)
  {
    console.log("loader",this.loader);
    let name = 'file';
    url='http://localhost:8080/loadimage';
    let formData:FormData = new FormData();
    let headers = new Headers();
    name = file.name;
    console.log("file name"+name);
    formData.append('attachment', file, name);
    const dotIndex = name.lastIndexOf('.');
    const fileName  = dotIndex>0?name.substring(0,dotIndex):name;
    formData.append('name', file.name);
    formData.append('source', user);
    console.log('formData',formData);
    headers.append('Content-Type', 'multipart/form-data');
    headers.set('Accept', 'application/json');
   console.log("Headers",headers);
    let params = new HttpParams();
    const options = {
        params: params,
        reportProgress: true,
    };
//http post return an observer
//so I need to convert to Promise
console.log("URL   "+url);
    return this.http.post(url,formData,options);
  }
//implement the upload 
  upload() {
      let upload = new Promise((resolve, reject)=>{
        var myReader= new FileReader();
        this.loader['file'].then(
            (data)=>{

                this.uploadFile(data,this.url,'test')
                .subscribe(
                    (result)=>{
//resolve data formate must like this
//if **default** is missing, you will get an error
console.log("attachement  ",result,data);
                        resolve({ default: data.file })
                    },
                    (error)=>{
                        reject(data.msg);
                    }
                );
            }
        );
      });
      return upload;
  }
  abort() {
      console.log("abort")
  }
}