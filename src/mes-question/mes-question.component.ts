import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserServiceService} from '../service/user-service.service';
import {Question} from "../model/question";
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ChangeEvent } from '@ckeditor/ckeditor5-angular/ckeditor.component';
import SimpleUploadAdapter from '@ckeditor/ckeditor5-upload/src/adapters/simpleuploadadapter';
import {UploadAdapter} from '../mes-question/UploadAdapter'; 
import { HttpParams, HttpClient } from "@angular/common/http";

@Component({
  selector: 'app-mes-question',
  templateUrl: './mes-question.component.html',
  styleUrls: ['./mes-question.component.css']
})
export class MesQuestionComponent implements OnInit 
{
public Editor = ClassicEditor;
private showForm:boolean;
private fileData:File=null;
private data:File;
private question:Question;
private editorData;
  constructor(private route: ActivatedRoute, 
     	private router: Router, 
        private userService: UserServiceService,
        private http:HttpClient
        ) 
  	{
  		this.showForm=false; 
  		this.question=new Question();
  		
  	}
  	public onReady( $editor ) 
  	{
        $editor.ui.getEditableElement().parentElement.insertBefore(
            $editor.ui.view.toolbar.element,
            $editor.ui.getEditableElement()
        );
        $editor.plugins.get('FileRepository').createUploadAdapter = (loader)=> 
        {
      		return new UploadAdapter(loader,'d',this.http);
    	};
    }

  ngOnInit() {
  }

  public toggleForm()
  {
  	if(this.showForm)
  		this.showForm=false; 
  	else
  		this.showForm=true;
  }

   public getData( { editor }: ChangeEvent)
   { 
   		

   		//this.fileData = <File>fileInput.target.files[0];
   		
   }
  onSubmit()
  { 
  	var today = new Date();
  	var dd = String(today.getDate()).padStart(2, '0');
  	var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  	var yyyy = today.getFullYear();

	  //today = mm + '/' + dd + '/' + yyyy;

  	this.question.date=mm + '/' + dd + '/' + yyyy;
  	this.question._id_user=JSON.parse(localStorage.getItem('user'))._id;
  	this.userService.saveQuestion(this.question).subscribe(result =>
  			{
  				console.log(result); 

  			}); 
  }


}
