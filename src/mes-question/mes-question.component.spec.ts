import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MesQuestionComponent } from './mes-question.component';

describe('MesQuestionComponent', () => {
  let component: MesQuestionComponent;
  let fixture: ComponentFixture<MesQuestionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MesQuestionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MesQuestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
