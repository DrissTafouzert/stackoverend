import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserServiceService} from '../service/user-service.service';
import {User} from '../model/user';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent {

  user: User;
  passwordConf:Boolean;
  password2:string;
  constructor(
    private route: ActivatedRoute, 
      private router: Router, 
        private userService: UserServiceService) 
  {
    this.user = new User();
    this.passwordConf=true;
  }
 
  onSubmit() 
  {
    this.userService.save(this.user).subscribe(result => this.gotoUserList());
  }
 
  gotoUserList() 
  {
    this.router.navigate(['/users']);
  }
  changed(e)
  {
  	//e.target.data
  	console.log(e.target.value,this.user.password)
  	if(e.target.value==this.user.password)
  	{
  		this.passwordConf=false;
  	}
  	else
  	{
  		this.passwordConf=true;	
  	}
  }
   

}
