import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../service/user-service.service';
import {Question} from "../model/question";

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit 
{
	private questions:Question[];
  constructor(private userService:UserServiceService) 
  {

  }

  ngOnInit() 
  {
  	this.userService.getAllQuestion().subscribe(data=>{this.questions= data;});
  }

}
