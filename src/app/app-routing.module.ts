import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserListComponent} from '../user-list/user-list.component';
import {UserFormComponent} from '../user-form/user-form.component';
import {UserLogin} from '../user-login/user-login.component';
import {MesQuestionComponent} from '../mes-question/mes-question.component';
import {AccueilComponent} from '../accueil/accueil.component';

const routes: Routes = [
	{path:'users',component:UserListComponent},
	{path:'addUser',component:UserFormComponent},
	{path:'login',component:UserLogin},
	{path:'mesquestion',component:MesQuestionComponent},
	{path:'accueil',component:AccueilComponent}

	];
	

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
