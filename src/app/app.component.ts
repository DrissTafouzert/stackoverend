import { Component } from '@angular/core';
import {UserServiceService} from '../service/user-service.service';
import {SessionUser} from '../sessionUser';
import {User} from '../model/user';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent 
{
	user:User;

	constructor(
        private userService: UserServiceService,
         private route: ActivatedRoute, 
      	private router: Router,
        public sessionUser:SessionUser) 
  	{ 
  		this.user = this.userService.getUser(); 
  		console.log("GETUSER from login",this.user);
  		console.log("localStorage############ ",localStorage.getItem('user'));
  		if(localStorage.getItem('user'))
  		this.user=JSON.parse(localStorage.getItem('user'));
  	}
  title = 'stackover';

  	logOut()
  	{
  		localStorage.setItem('user',null);
  		this.user=null;
  		this.router.navigate(['/']);

  	}

}
