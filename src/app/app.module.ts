import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {UserListComponent} from '../user-list/user-list.component';
import {UserFormComponent} from '../user-form/user-form.component';
import {UserLogin} from '../user-login/user-login.component';
import {MesQuestionComponent} from '../mes-question/mes-question.component';
import {UserServiceService} from '../service/user-service.service';
import {SessionUser} from '../sessionUser';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';
import {AccueilComponent} from '../accueil/accueil.component';


@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserFormComponent,
    UserLogin,
    MesQuestionComponent,
    AccueilComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CKEditorModule
  ],
  providers: [UserServiceService,SessionUser],
  bootstrap: [AppComponent]
})
export class AppModule { }
