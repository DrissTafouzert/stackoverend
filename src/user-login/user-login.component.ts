import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserServiceService} from '../service/user-service.service';
import {User} from '../model/user';
import {SessionUser} from '../sessionUser';
@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLogin implements OnInit 
{
	user: User;
  	constructor(
  		private route: ActivatedRoute, 
     	private router: Router, 
        private userService: UserServiceService,
        public sessionUser:SessionUser) 
  	{ this.user = new User(); }

  	onSubmit()
  	{
  		this.userService.login(this.user.email,this.user.password).subscribe(result =>
  			{
  				console.log(result); 
  				//this.sessionUser.user.name=result['name'];
  				localStorage.setItem("user",JSON.stringify(result));
  				//this.sessionUser.name="result['admin']";
  				
				  
  				this.gotoUserList();

  			}); 
  	}

  	gotoUserList()
  	{
  		this.router.navigate(['/']).then(() => 
  		{
			window.location.reload();
		});;
  	}
  	ngOnInit() {
  	}

} 
