import { Injectable } from '@angular/core';
import {User} from './model/user';

@Injectable()
export class SessionUser 
{
  user:User;
  name:string;
}