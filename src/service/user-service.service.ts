import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import {User} from '../model/user';
import {Observable} from 'rxjs';
import {Question} from '../model/question';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService 
{
	private getAllUser: string;
	private saveUser: string;
  private loginUrl:string;
  private mesQuestionUrl:string;
  private saveQuestionUrl:string;
  private getAllQuestionUrl:string;
  private user:User;

  constructor(private http: HttpClient)
  {
  	this.saveUser="http://localhost:8080/addUser";
  	this.getAllUser="http://localhost:8080/users";
  	this.loginUrl="http://localhost:8080/login";
    this.mesQuestionUrl="http://localhost:8080/loadimage";
    this.saveQuestionUrl="http://localhost:8080/saveQuestion2";
    this.getAllQuestionUrl="http://localhost:8080/getAllQuestion";
  }

  public findAll():Observable<User[]>
  {
  	return this.http.get<User[]>(this.getAllUser);
  }
  
  public save(user:User)
  {
  	return this.http.post<User>(this.saveUser,user);
  }
  
  public login(email:string, password:string)
  {
    const params = new HttpParams()
    .set('email', email)
    .set('sort', password);
    
  	return this.http.get(this.loginUrl,{params});
  }
  
  public sendQuestion(data:File)
  {
    let formData:FormData = new FormData();
    let headers = new Headers();

    formData.append('attachment', data); 
    formData.append('name', "driss"); 
    headers.append('Content-Type', 'multipart/form-data');
    headers.set('Accept', 'application/json'); 
    
    let params = new HttpParams();
    const options = {
        params: params,
        reportProgress: true,
    };

    return this.http.post(this.mesQuestionUrl,formData,options);
  }

  public saveQuestion(question:Question)
  {
    console.log(question);
     return this.http.post<Question>(this.saveQuestionUrl,question);
  }
  public getAllQuestion():Observable<Question[]>
  {
    return this.http.get<Question[]>(this.getAllQuestionUrl);
  }
  public logOut()
  {
    this.user=null;
  }

  public setUser(user:User)
  {
    this.user=user;
  }

  public getUser()
  {
    return this.user;
  }
}
